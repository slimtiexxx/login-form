function loginDirective() {

    // Definition of directive
    var directiveDefinitionObject = {
        restrict: 'E',
        templateUrl: '../components/login-form.html'
    };

    return directiveDefinitionObject;
}

module.exports = loginDirective;