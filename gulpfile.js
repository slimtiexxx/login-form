'use strict';

var path            = './';
var browser         = 'google chrome';
var port            = 3000;

var gulp            = require('gulp');
var sass            = require('gulp-sass');
var browserify      = require('gulp-browserify');
var browserSync     = require('browser-sync');
var sourcemaps      = require('gulp-sourcemaps');
var postcss         = require('gulp-postcss');
var uglify          = require('gulp-uglify');
var cssnano         = require('gulp-cssnano');

// Concat scripts
gulp.task('scripts', function() {
    gulp.src(path + 'app/app.js')
        .pipe(browserify())
        .pipe(uglify())
        .pipe(gulp.dest(path + 'assets/js'));
});

// Compile SASS
gulp.task('sass', function () {
    return gulp.src(path + 'scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(postcss([ require('precss'), require('autoprefixer') ]) )
        .pipe(cssnano())
        .pipe(sourcemaps.init())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(path + 'assets/css'));
});


// Static server + watching files
gulp.task('serve', ['sass', 'scripts'], function() {

    browserSync.init({
        server: path,
        browser: browser,
        port: port
    });

    gulp.watch([
        path + "app/*.js",
        path + "components/*.js"
    ], ['scripts']);

    gulp.watch([
        path + "scss/*.scss",
        path + "scss/**/*.scss"
    ], ['sass']);

    gulp.watch([
        path + "*.html",
        path + "**/*.html",
        path + "assets/css/*.css",
        path + "assets/js/*.js"
    ]).on('change', browserSync.reload);
});
