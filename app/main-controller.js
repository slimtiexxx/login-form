function MainController($scope){
    $scope.formSubmit = function(e){
        e.preventDefault();
        var data = $scope.fields;

        $scope.response = data;
    };
}

MainController.$inject = ['$scope', '$http', '$rootScope', '$location'];

module.exports = MainController;