
function config($routeProvider, $locationProvider) {

    // routes
    $routeProvider
        .when('/', {
            templateUrl: 'views/login.html'
        })
        .otherwise({
            redirectTo: '/'
        });

    $locationProvider.html5Mode(false);

}

// Config Injections
config.$inject = ['$routeProvider', '$locationProvider'];

module.exports = config;