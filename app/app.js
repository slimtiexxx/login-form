var angular = require('angular');

// Import Config
var config = require('./config');

// Import Controllers
var MainController = require('./main-controller');

// Import Directives
var loginDirective = require('../components/login.directive.js');

// Import Dependencies
require('angular-route');


angular
    .module('login', [
        'ngRoute'
    ])

    // Config inject
    .config(config)

    // Controllers inject
    .controller('MainController', MainController)

    // Directives inject
    .directive('loginDirective', loginDirective);
